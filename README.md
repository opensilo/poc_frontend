# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

####Steps to run project on localhost ####
* Step 1 :  `cd to frontend_POC/`
* Step 2 : `npm install --save-dev`
* Step 3 : `bower install --save-dev`
* Step 4 :  `grunt serve` to start live reload on `http://localhost:9000`

####Steps to make a minified build and upload on CDN 
* Step 1 : Run command `grunt` in the project root dir to make build, If build fails because of sytax warning than you must remove those warning by resolving with correct syntax. 
Note: If build process fails because of failed unit tests than run `grunt --force` it will force fully make a build in distributed folder i.e. `dist/`. 
* Step 2 : `cd dist/`
* Step 3 : Run `firebase init` will initialize the firebase hosting after authentication and will prompt the list of firebase apps on which you want to host.
* Step 4 : Run `firebase deploy` will deploy the code on CDN.
* Step 5 : After successfully deploy you can open the hosted CDN via `firebase open`.

This how our project development environment works, Let me if you face any issues while configuring the setup.

Thanks,
Abdul Wahid